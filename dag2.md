Dag 2
=====

## Web server and applikation server
En webbserver levererar statisk innehåll som finns i dess filsystem mha HTTP protocol. En webserver har ingen support för transaktioner eller databas kopplingar.
En webbklient kommunicerar med webserver via , http. Denna http protokoll är stateless och connectionless.

Exempel på webbservers:
* apache web server
* microsoft IIS
* nginx
* jetty
* apache tomcat

En applikationsserver leverar bussinesslogic (data och metod anrop) till klient applikationer via diverse protokoll. En applikationsserver kan leverera både statisk och dynamisk innehåll. En applikationsserver har stöd för transaktioner, databas kopplingar, säkerhet och resursallokering.

Exempel på applikationsservers:
* Apache Geronimo
* JBoss
* Windows 2000
* Weblogic
* TomEE
* Glassfish

De flesta applikationsservrar innehåller också en webbserver, vilket innebär att du kan betrakta en webbserver  som en del av en applikationsserver.

#### Studiematerial
* The Http and the Web | Http Explained | Request-Response Cycle (8:54 min)
	* https://www.youtube.com/watch?v=eesqK59rhGA 
* Whats the difference between Web and App Server? (7:19 min)
	* https://www.youtube.com/watch?v=S97eKyv2b9M 
* What is a cookie? (4:34 min)
	* https://www.youtube.com/watch?v=I01XMRo2ESg


#### Uppgift
- Skapa en helloworld html sida som web server skall leverera.
- Installera nginx
- Installera jetty
- Installera apache web server

## IDE
En utvecklingsmiljö (engelska Integrated Development Environment, IDE), är ett datorprogram eller en programsvit som vanligtvis innehåller en textredigerare, kompilator, och debugger, tillsammans med ett antal andra funktioner avsedda att underlätta vid programmering.

Exempel på IDE:
* KDevelop
* NetBeans
* Eclipse
* Delphi
* Microsoft Visual Studio
* IntelliJ

#### Uppgift
- Installera Eclipse
- Installera Netbeans
- Installera IntelliJ

## Webbutveckling
Webbutveckling är en samlingsterm för allt runtomkring uppbyggnaden av en webbplats. Det inkluderar programmering av server/klient-sidan, webbdesign, systemutveckling, datasäkerhet, uppbyggnad av databaser och många andra saker. Webbutveckling kan vara allt ifrån att skriva en textfil med HTML till att skapa komplexa webbapplikationer.

##### Tools
* Webbklient, tex en webläsare, firefox, chrome
* curl
* Dev-tool plugin, webdeveloper (firefox), Postman (chrome)
* "Inspect element" (firefox) / "Inspect" (chrome)


#### Studiematerial
* The Internet: HTTP and HTML (7:06 min)
	* https://www.youtube.com/watch?v=kBXQZMmiA4s 

#### Uppgifter
- Prova html grunder online; 
	- https://www.codeavengers.com/profile#ht1
	- https://www.codecademy.com/learn/web
	- https://www.codecademy.com/learn/learn-html-css
	- https://www.codecademy.com/learn/make-a-website

#### Projekt
habofa-projektet, använd valfri IDE & valfrit web server.
