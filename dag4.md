Dag 4
====

## Front-end programmering med blocks



#### Studiematerial
Skapa ett konto på https://code.org

- https://code.org/mc

#### Uppgift
Gå igenom
- https://studio.code.org/flappy/1
- https://code.org/starwars
- Se intro & demo längre ner på sidan.
	- https://code.org/educate/applab
- Bygg om habofa i Blocks
