Material till PRAO i Systemutveckling
=====

## 1.Förutsättningar
* E-post address, https://accounts.google.com/SignUp
* Dator med admistrationsrättigheter
* Webbläsare Chrome installerat
* Virtual Box installerat
* Ubuntu 16.04 VB image

## Web Tools
* curl
* chrome + plugins

## Network Tools
* PING and TRACERT (traceroute) networking commands
 	- https://www.youtube.com/watch?v=vJV-GBZ6PeM
* Network Troubleshooting using PING, TRACERT, IPCONFIG, NSLOOKUP COMMANDS
 	- https://www.youtube.com/watch?v=AimCNTzDlVo