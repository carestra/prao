Dag 3
=====

## Databas
Data är uppgifter av olika slag. Ibland skiljer man data från information, som är data som man gett en tolkning. Alltså är 23 ett exempel på data, medan det är information om vi vet att det är 23 grader varmt ute. 

Med ordet databas brukar man mena

* en samling data som hör ihop,
* som modellerar en del av världen, till exempel ett företag och dess verksamhet,
* och är persistent, dvs inte försvinner när man avslutar programmet eller stänger av datorn. 

En relationsdatabas är en databas där information ("data") är organiserad i relationer (även kallade tabeller), eventuellt med restriktioner inom en och samma relation eller mellan relationer.

Det finns program som har till uppgift att lagra och hantera databaser. Ett sådant program kallas för databashanterare, databashanteringssystem eller DBHS. På engelska kallas det database management system och förkortas DBMS. 

Exempel på databashanterare

* Microsoft Access
* Microsoft SQL Server
* Postgress
* Mysql
* Oracle
* Ingress

##### Studiematerial

- https://www.codeschool.com/courses/try-sql
- https://www.codecademy.com/learn/learn-sql
- http://sqlzoo.net/

##### Upgifter
- Installera valfri open source databas
- Modellera en databas schema till projektet habofa.
- Skapa scheman med respektive tabeller i databasen.
- Skapa innehåll till databasen via databasklienten.

## Java
Java är ett objektorienterat programspråk utvecklat av Sun Microsystems. En stor fördel med Java är att program skrivna i Java är nästan platformsoberoende. Du kan alltså köra dem i alla miljöer till vilka det finns en 'javamotor'.

Man skapar ett Java-program i flera steg:

- Man börjar med att tillverka en källfil. Källfilen består av vanlig text som följer Javas syntax.
- När källfilen är klar måste den kompileras av en kompilator till en bytekodfil. Bytekodfilen kan läsas av Java Virtual Machine (JVM), en emulerad dator eller exekveringsmiljö.
- Varje dator som kör programmet använder en programtolkare som tar JVM till hjälp för att tolka bytekoden till instruktioner som datorn kan utföra.

1 Källfil HelloWorld.java

	public class HelloWorld {
    	public static void main(String[] args) {
      		System.out.println("Hello World!");
    	}
	}

2 Kompila källfil: javac HelloWorld.java
byte-kod

3 Kör programmet: java HelloWorld


#### Studiematerial
- Se filen 'Grunderna_i_Java.pdf'
- Kör online tutorial på
	- https://www.codecademy.com/learn/learn-java

##### Uppgifter
Se https://programmingbydoing.com/

Programmera dessa uppgifter lokal mha av valfrit IDE

- 3, 4, 7, 11, 19, 22, 42 (	Magic 8-Ball), 44 (Fortune Cookie)


############## TO be remove - don't do!#############
## Web services
En web service eller webtjänst är ett system för att utbyta information och tjänster mellan maskin-till-maskin över ett nätverk. I en webbapplikations kontext refererar det vanligen till en uppsättning API:er som kan kommas åt över internet och exekveras på ett fjärrsystem som erbjuder den begärda tjänsten.
De tekniker som används i web service är SOAP, REST, and XML-RPC.

### API
API är en publicerad gränssnitt som definierar hur komponent A kommunicerar med komponent B.

En webbtjänst är en form av API där gränssnittet definieras med hjälp den teknik som har valds för web service. Detta tillåter fjärrsamtal av ett gränssnitt via HTTP.

Publika API, http://apikatalogen.se/

#### Studiematerial
* https://www.youtube.com/watch?v=7YcW25PHnAA


