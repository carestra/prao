Dag 1
====
## 1 Förutsättningar
- Dator med admistrationsrättigheter alternativ en virtuell maskin och en image med valfrit OS.
- Internet uppkoppling, kabel eller wifi

## Internet grunderna

### Historik

* Före år 1957 så arbeta alla datorerna med 'Batch processing' - one task at the time
* Computer size - cool rooms without direct access to the computer
* Remote connection to main frame to access computer
* Time-sharing - share computer power with multiple users
* 1962 föreslags ett system för att få datorer att prata med varann.
* 1969 första meddelande som skickas genom ARPANET
* 1971 utvecklas det första systemet för att skicka epost.
* 1991 uppfann the World Wide Web som bygger på 3 fundamentala tekniker, html (HyperText Markup Language), uri (Uniform Resource Identifier), http (Hypertext Transfer Protocol).
* 1992 kom det första webläsaren
* 1994 Netscape Navigator populärast webbläsare
* 1997 Google skapas
* ??? Sökmotorstid Altavista, Yahoo
* 1999-2001 Dot-com boom
* 2004 - The cloud, Internet of tings, IoT

#### Studiematerial
- History of the Internet (8min)
	- https://www.youtube.com/watch?v=9hIQjrMHTv4
- HISTORY OF THE INTERNET (3min)
	- https://www.youtube.com/watch?v=h8K49dD52WA
- What is the Internet? (3min)
	- https://www.youtube.com/watch?v=Dxcc6ycZ73M
	
#### Quiz
* Vad är batch processing
* Vad är ARPANET
* Vad kännetecknas en centraliserad arkitektur gentemot en decentraliserad (distribuerat) arkitektur
* Vem utvecklade första systemet för att skicka mail
* Vem föreslåg ideen att låta datorer prata med varann
* Vad skickades som första meddelande i ARPANET.
* Vad står www för
* Vad är erwise
* Vilka är Vint Cerf och Bob kahn
* Vem styr Internet
* Vad är en webbläsare
* Hur skickar man epost i Internet

## Hur funkar Internet
* Modem, ADSL (ethernet kabel), fiber (glassfiber) anslutningar
* ISP (Internet Service Provider)
* Statisk / Dynamik IP
* LAN (Local Area Network)
* Routers, Backbone
* Domain Name Server (DNS)
* Enhet 1 bit, 1 byte
* Transmission capacity, bandwith
* firewall
* VPN
* curl, ping, traceroute, nslookup

#### Studiematerial
- How the Internet Works (5min)
 	- https://www.youtube.com/watch?v=7_LPdttKXPc
- How Does the Internet Actually Work? (3:40min)
	- https://www.youtube.com/watch?v=ZonvMhT5c_Q
- The Internet: Wires, Cables, & Wifi (6:30min)
	- https://www.youtube.com/watch?v=ZhEf7e4kopM
- The Internet: Packets, Routing and Reliability (6:23min)
	- https://www.youtube.com/watch?v=AYdF7b3nMto
- The Internet: IP Addresses and DNS (6:40)
	- https://www.youtube.com/watch?v=5o8CwafCxnU
- How Vpn Work (1:48min)
	- https://www.youtube.com/watch?v=plvWpky92eU
	
Extra material:

- DNS Explained (5:45min)
	- https://www.youtube.com/watch?v=72snZctFFtA
- Hub, Switch or Router? Network Devices Explained (7:40min)
 	- https://www.youtube.com/watch?v=Ofjsh_E4HFY
- How a VPN Works (7:28)
	- https://www.youtube.com/watch?v=zpUHD-PQuZg

#### Quiz
* Vad är Internet?
* ISP står för?
* vad är en IP adress (publik och privat)
* Vad är en server respektive en klient?
* Vad är en paket?
* Vad är en router
* vad är bandwith?
* Vad är en binärkod
* Vad är bitrate?
* Vad är ett protokoll?
* Vad gör en DNS?

#### Uppgifter
- Installera minst 3 olika webbläsare. Vad skijer de åt?
- Hur kan man se vart en webbsida är ifrån?
- Hur tar man reda på vilken IP ens dator har?
- Behöver man en webbläsare för att hämta en webbsida?
- Om man inte har nätverk, hur tar man reda på vart felet ligger?

## Datorn så funkar den
#### Historik
* 1100-1532 f.kr. Quipu - eller khipu var trådknippen som användes som informationsbärare i Inkariket och äldre kulturer i Anderna. En quipu består vanligen av färgad spunnen och flätad tråd av lama- eller alpackahår eller bomullssnören med numeriska och andra värden kodade med knutar i ett decimalsystem. 
* 300 f.kr. Abacus första hjälpmedel för matematiska beräkningar
*  1623 konstrueras den första mekaniska räknemaskinen, calculating clock.
*  1642 byggs en kuggdriven räknare som endast kunde addera tal.
*  1801 uppfunns den automatiska Jacquard-vävstolen (eng. loom), vilket var den första tillämpningen av hålkort och lagrad instruktion. Genom att förse vävstolen med en hålkortsläsare kunde den programmeras att väva ett visst mönster med minsta möjliga mänskliga inblandning, vilket innebar att en enda vävare kunde utföra ett arbete som tidigare krävde en insats av flera. Införandet av Jacquard-vävstolarna var en utlösande faktor i den industriella revolutionen
*  1822 Differentialmaskinen var namnet på en maskin som Charles Babbage konstruerade men aldrig byggde färdigt. Maskinen var avsedd att beräkna matematiska tabeller med en matematisk metod som kallas differensmetoden.
* 1890 Utförde en belöning till den som kunde skapa en maskin för att utföra folkräkning/Census.
En hushålls- och bostadsräkning, även kallad Census, innebär att samla in uppgifter om hushåll, boende och bostäder i ett land vid en given tidpunkt.
Herman Hollerith uppfinner hålkortsmaskinen (tabulating machine)
* 1936 Konrad Zuse, använder elektromekarisk relä till binärmodell
* 1936 Turing maskin beskrev hur en maskin kunde lösa varje logik problem möjlig.
* 1945 Beskrev hur en generell maskin kunde skapas för att lösa olika typer av uppfigter.
aritmetikenhet, minne, styrenhet och användargränssnitt (I/O), arbeta binär tal, elektronisk och sekventiell operation.
* 1943 ENIAC skapades, den var inte en binär maskin, den räknade med det decimala talsystemet.
* 1948 färdigställdes den första digitala programlagringsbara datorn. Manchester Small-Scale Experimental Machine (SSEM).
* 1951 det första kommersiella dator färdigställdes, UNIVAC I (UNIVersal Automatic Computer I)
* 1957 FORTRAN, Första högnivå programmeringsspråk
* 1971 Intel 404 det första kommersiella mikroprocessorn.
* 1971 Programmeringsspråket C skapades
* 1974 Altair 8800 första mikrodator
* 1979 C++ skapades
* 1980 MS-Dos skapades
* 1981 IBM PC släpptes
* 1982 Commadore 64 släpptes
* 1984 The Macintosh släpptes
* 1984 Mac OS skapdes
* 1985 Microsoft läpptes
* 1990 html

#### Studiematerial
- The History of Computers (10:25min)
	- https://www.youtube.com/watch?v=VMuQppYtTCo
- History of computers - A Timeline (3:32min + 3:08min)
	- https://www.youtube.com/watch?v=pBiVyEfZVUU
	- https://www.youtube.com/watch?v=HRi1BHjID3o
- A Brief History of the Computer (5:53min)
	- https://www.youtube.com/watch?v=97HvcEPHsyI
- Learn Computer Programming - Binary Basics (2:26min)
	- https://www.youtube.com/watch?v=u3IvvgRcAKY
- How Computers Work: Information (Part I) (12min)
	- https://www.youtube.com/watch?v=AtqjxyV9t1I

Extra material

- Charles Babbage, Konrad Zuse and the Computer (Milestones of Science) (14:48min)
	- https://www.youtube.com/watch?v=-ReL9JnWA1A
- How Computers Work: Computation (Part II) (15min)
	- https://www.youtube.com/watch?v=9a7XQmEBK5Q
- How a CPU Works (20min)
	- https://www.youtube.com/watch?v=cNN_tTXABUA

#### Quiz
* Vem är Wilhelm Schickard?
* Vem är Joseph-Marie Jacquard?
* Vad är IBM?
* Vem är Grace Hopper?
* Vad betyder debugging?
* Vem/vad Colossus?
* Vad är ENIAC?
* Vad är mikroprocessor?
* VAd är en integrerad kretskort?
* vad är en mikrodator?
* Vem är John von Neumann?
* vem är Alan Mathison Turing? 

## Binär talsystem
* 1bit, 4 bits, 8bits=8Bit (0-255 tecken)
* 8|4|2|1 => 0|1|1|0 => 2^3|2^2|2^1|2^0

#### Studiematerial
- See How Computers Add Numbers In One Lesson (14:26min)
	- https://www.youtube.com/watch?v=VBDoT8o4q00

#### Quiz
* hur skriver man 20 i binärform? Noll? 100?
* Hur skiver man 20 i 4bitar, i 1Byte?


## Operativsystem
* hårdvara - devices
* mjukvara - programs
* OS - load and manage processes, interfaces to hardware via system calls, filsystem, shell (basic user interface)
* https://en.wikipedia.org/wiki/Shell_(computing)#/media/File:Linux_kernel_and_gaming_input-output_latency.svg
* Enkelt uttryckt, skalet är ett program som tar kommandon från tangentbordet och ger dem till operativsystemet för att utföra. Förr i tiden var det enda användargränssnittet som finns på en Unix-liknande system som Linux. Numera har vi grafiska användargränssnitt (GUI) utöver kommandorad gränssnitt (CLI) såsom skalet.


#### Studiematerial
- Operating Systems 1 - Introduction (3:36min)
	- https://www.youtube.com/watch?v=5AjReRMoG3Y
- How Operating Systems Work (2min)
	- https://www.youtube.com/watch?v=tPDyxtpxPcY

Extra material

- Operating systems basic (23min)
	- https://www.youtube.com/watch?v=9GDX-IyZ_C8

#### Quiz
- Vad är ett program?
- Vad är ett OS?
- Vilka komponenter består en vanlig dator av?
- är en mobilenhet en dator? om ja, vilka delar är mappar mobilen till datorn? om inte, vad är skillnad?
- Är en mikrovågsugn en dator? förklara!

#### Uppgifter
- Logga in på datorn, identifera alla de komponenter som ingår i vonNewman arkitekturren.
- Vad behövs för att en hårdvara ska kunna kopplas till datorn? Kan man styra den direkt?

- Prova terminalen online
	- https://www.codecademy.com/courses/learn-the-command-line

- Prova följande från din dator
	- Öppna en terminal och gå till din hemkatalog.
	- I din hemkatalog, skapa en mapp, kalla den för repo.
	- Gå till mappen/katalogen ~/repo och skapa mappen uppgifter
	- I mappen ~/repo/uppgifter skapa en tom fil (emptyfile).
	- I mappen ~/repo/uppgifter skapa en fil (helloWorld.txt) med följande text:
	Hello Worl!
	- Lista innehållet i mappen ~/repo samt lista allt i dess under kataloger.
	- Lista innehållet i filen ~/repo/uppgifter/helloWorld.txt
	- Kopiera file ~/repo/uppgifter/helloWorld.txt till din hemkatalog
	- Ta bort filen ~/repo/helloWorld.txt

## Versionshantering
- Concurrent versions system (CVS) är ett system för versionshantering.
- CVS håller reda på historiken över förändringar av filer i ett projekt och tillåter flera användare att arbeta tillsammans från olika platser.
- Olika typer, git mercurial, svn, ...

### Git

Git är ett versionshanteringsprogram som skapades 2005 för att hantera källkoden till Linuxkärnan.

Git är ett distribuerat versionshanteringssystem, vilket innebär att ett centralt arkiv saknas, och att vem som helst kan skapa en egen kopia. Vissa arkiv kan sägas vara viktigare än andra, till exempel det arkiv där den officiella Linuxversionen publiceras, men det är inte av tekniska skäl utan rent sociala konventioner.

Git är i grunden kommandoradbaserat (CLI).

##### Github
Github är ett webbhotell för mjukvaruutvecklingsprojekt som använder versionshanteringssystemet Git.

##### Gitlab
Gitlab är ett webbhotell för mjukvaruutvecklingsprojekt som använder versionshanteringssystemet Git med wikki och ärendehantering.

#### Studiematerial
- Git Tutorial for Beginners 2 How to Install Git (10:29-18:24)
	- https://www.youtube.com/watch?v=iIjl4mj4AMc
- Git Tutorial for Beginners 3 Getting Started (1:08-4:36)
	- https://www.youtube.com/watch?v=xH27nr7iaZM

#### Uppgifter
- Skapa ett konto på
	- https://www.codeschool.com/users/sign_up
	- https://www.codeavengers.com/
	- https://www.codecademy.com/register?redirect=https%3A%2F%2Fwww.codecademy.com%2Fcourses%2Flearn-git%2Flessons%2Fgit-workflow%2Fexercises%2Fhello-git%3Faction%3Dlesson_resume
- Prova git online; om de frågar efter din epost ange en påhittat en
	- https://try.github.io/levels/1/challenges/1 (15min)
	- http://git.rocks/getting-started/
	- https://www.codecademy.com/courses/learn-git/lessons/git-workflow/exercises/hello-git?action=lesson_resume
- Prova följande från din dator
	- Installera Git i din datorn
	- Konfigurera git.
	- Börja versionshantera katalogen ~/repo/uppgifter
	- I mappen ~/repo/uppgifter kör följande:
		ls > minfile
	- Lägg filen "minfile" under versionshantering, dvs add + commit.